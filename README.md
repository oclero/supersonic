<div align="center">
	<img height="92" src="icon.png">
</div>
<div align="center">
<h1>Supersonic - Hugo theme</h1>
</div>

![License: MIT](https://img.shields.io/badge/license-MIT-green)

**Supersonic** is a very basic [Hugo theme](https://gohugo.io) for my own personal website at [www.olivierclero.com](https://www.olivierclero.com).

It was not aimed to be used by people other than me... But hey! If you want to play with it, have fun.

## How to install

1. Use a git submodule for the theme official repository, or fork it and use a git submodule for your own fork.

   ```sh
   cd path/to/your/site
   git submodule add git@gitlab.com:oclero/supersonic.git themes/supersonic
   ```

2. In your `hugo.yaml` file, add:

   ```yaml
   theme: supersonic
   ```

## How to customize

### Hugo config

Your config must look like this:

```yaml
baseURL: https://www.your-url.com/
languageCode: en-us
title: Your Title
theme: supersonic
pluralizelisttitles: false
disableKinds:
  - RSS
markup:
  highlight:
    anchorLineNos: false
    guessSyntax: false
    style: monokai
    tabWidth: 2
    lineNos: true
    lineNoStart: 1
taxonomies:
  tag: tags
params:
  author: ''
  location: ''
  description: ''
  preamble: /static/preamble.md
  mastodon: '' # optional
  twitter: '' # optional
  gitlab: '' # optional
  linkedin: '' # optional
  email: '' # optional
  avatar: /img/avatar.jpg
  favicon: /img/favicon.png
```

#### Description

| Key           | Value                         | Role                                         |
| :------------ | :---------------------------- | :------------------------------------------- |
| `author`      | Your name                     | Used in Hero and footer copyright.           |
| `location`    | The place you live            | Used in Hero.                                |
| `description` | Your website's description    | Metadata.                                    |
| `preamble`    | `/static/path/to/preamble.md` | Small text in the Hero.                      |
| `mastodon`    | `'@username@instance.ext'`    | Your Mastodon profile.                       |
| `twitter`     | `username`                    | Your Twitter username.                       |
| `gitlab`      | `username`                    | Your Gitlab username.                        |
| `linkedin`    | `username`                    | Your LinkedIn username.                      |
| `email`       | `your.email@domain.ext`       | Your email. See [further](#contact-page).    |
| `avatar`      | `/path/to/avatar.jpg`         | Avatar path, relative to your `/static` dir. |
| `favicon`     | `path/to/favicon.png`         | Favicon, relative to `assets` dir.           |

#### Remarks

- Favicon is generated in all different sizes automatically.

### Stuff that can't be done in config file

#### Background image

Put an image called `background.jpg` in your `/static/img` directory and it'll be picked up by the CSS theme.

#### Contact page

You have 3 options:

- If no email is provided, the email icon is not shown.

- If an email is provided, but no file called `contact.md` is found at the root of your `/content` dir, the email icon just links to `mailto:your.email@domain.ext`. Do it at your own risk (spambots).

- If email and `contact.md` are provided, and `contact.md` contains the shortcode `{{< contact >}}`, the email icon links to the `contact` page.

  Use this solution if you have a PHP server and you don't want your email to be public. The file `contact.php` will be copied to your server and will be used to send an email to you when someone submits the contact form.

  This PHP file must get information from a `contact_config.php` file that you add in your `/static/php` dir, that must contain your email:

  ```php
  <?php
  $address = 'your.email@domain.ext';
  ?>
  ```

  The contact form is not bullet-proof to spambots, but should be better than your email publicly visible. The form contains a honeypot for bots.
