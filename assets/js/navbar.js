'use strict';

document.addEventListener('DOMContentLoaded', function () {
	window.addEventListener('load', function () {
		const menuCheckbox = document.getElementById('menu-checkbox');
		const fixedElement = document.getElementsByTagName('body')[0];
		const noScrollCssClass = 'noScroll';
		menuCheckbox.addEventListener('change', (event) => {
			if (event.target.checked) {
				fixedElement.classList.add(noScrollCssClass);
			} else {
				fixedElement.classList.remove(noScrollCssClass);
			}
		});
	});
});
